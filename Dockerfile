FROM python:3.6-stretch
WORKDIR /opt/bot
COPY requirements.txt .
RUN pip install -r requirements.txt
CMD ["python", "bot.py"]