#!/usr/bin/python3
# coding=utf-8

# Default Imports
import json
import time
# import codecs

# custom Imports
import discord
import requests
from colorama import *


def time_HMS():
    return str(time.strftime("%H:%M:%S"))


def time_date():
    return str(time.strftime("%d.%m.%Y"))


# def log(message, log_type="INFO", log_file="bot.log"):
def log(message, log_type="INFO"):
    log_types = {
        "INFO": Style.BRIGHT + Fore.WHITE,
        "DEBUG": Style.BRIGHT + Fore.YELLOW,
        "WARNING": Style.BRIGHT + Fore.RED,
        "ERROR": Style.DIM + Fore.RED,
    }
    message = '[' + time_date() + " " + time_HMS() + " " + str(log_type) + "]: " + str(message)

    """
    with codecs.open(log_file, "a", "utf-8") as log_file:
        log_file.write(message + "\n")
        log_file.close()
    """

    print(log_types[log_type] + message)


class Rules_bot(object):
    def __init__(self, messages, logs, token,
                 kick_message_invite="You have been kicked from Oculus. Cause you disagreed the rules.\nIf you what to come back here is an invitation to the server: <invite> ",
                 kick_message_NO_invite="You have been kicked from Oculus. Cause you disagreed the rules.",
                 kick_reason="disagreed rules"):
        self.bot = discord.Client()
        self.token = token
        self.kick_message_NO_invite = kick_message_NO_invite
        self.kick_message_invite = kick_message_invite
        self.kick_reason = kick_reason
        self.logs = logs
        self.messages = messages

    def run(self):
        @self.bot.event
        async def on_raw_reaction_add(payload):
            if str(payload.guild_id) in self.messages:
                if payload.message_id == self.messages[str(payload.guild_id)]:
                    guild = discord.utils.find(lambda g: g.id == payload.guild_id, self.bot.guilds)
                    channel = discord.utils.find(lambda c: c.id == payload.channel_id, guild.text_channels)
                    member = discord.utils.find(lambda m: m.id == payload.user_id, guild.members)
                    role = discord.utils.get(guild.roles, name="Player")

                    if payload.emoji.name == "❌":
                        if str(payload.guild_id) in self.logs:
                            Disagreed = {
                                "username": "Rules",
                                "embeds": [
                                    {
                                        "title": "Disagreed:",
                                        "color": 16711680,
                                        "fields": [
                                            {
                                                "name": "Username:",
                                                "value": str(member.mention),
                                                "inline": True
                                            },
                                            {
                                                "inline": True,
                                                "name": "User ID:",
                                                "value": str(member.id)
                                            }
                                        ],
                                        "footer": {
                                            "text": time_date() + " " + time_HMS()
                                        },
                                        "thumbnail": {
                                            "url": str(member.avatar_url)
                                        }
                                    }
                                ],
                                "avatar_url": str(self.bot.user.avatar_url)
                            }
                            requests.post(url=self.logs[str(payload.guild_id)], json=Disagreed)
                        invite = None
                        try:
                            for i in await guild.invites():
                                if i.inviter.id == self.bot.user.id:
                                    invite = str(i.url)
                                    break
                        except discord.errors.Forbidden:
                            if str(payload.guild_id) in self.logs:
                                permissions = {
                                    "username": "Rules",
                                    "embeds": [
                                        {
                                            "title": "Permissions:",
                                            "color": 16776960,
                                            "footer": {
                                                "text": time_date() + " " + time_HMS()
                                            },
                                            "description": 'i am Missing the Permissions "manage_guild"!'
                                        }
                                    ],
                                    "avatar_url": str(self.bot.user.avatar_url)
                                }
                                requests.post(url=self.logs[str(payload.guild_id)], json=permissions)
                        if not invite:
                            try:
                                invite = str(await channel.create_invite())
                            except discord.errors.Forbidden:
                                if str(payload.guild_id) in self.logs:
                                    permissions = {
                                        "username": "Rules",
                                        "embeds": [
                                            {
                                                "title": "Permissions:",
                                                "color": 16776960,
                                                "footer": {
                                                    "text": time_date() + " " + time_HMS()
                                                },
                                                "description": 'i am Missing the Permissions "create_instant_invite"!'
                                            }
                                        ],
                                        "avatar_url": str(self.bot.user.avatar_url)
                                    }
                                    requests.post(url=self.logs[str(payload.guild_id)], json=permissions)
                        try:
                            if invite:
                                await member.send(self.kick_message_invite.replace("<invite>", str(invite)))
                            else:
                                await member.send(self.kick_message_NO_invite)
                        except (discord.Forbidden, discord.HTTPException):
                            pass
                        try:
                            await member.kick(reason=self.kick_reason)
                        except discord.errors.Forbidden:
                            if str(payload.guild_id) in self.logs:
                                permissions = {
                                    "username": "Rules",
                                    "embeds": [
                                        {
                                            "title": "Permissions:",
                                            "color": 16776960,
                                            "footer": {
                                                "text": time_date() + " " + time_HMS()
                                            },
                                            "description": "I am Missing Permissions to kick"
                                        }
                                    ],
                                    "avatar_url": str(self.bot.user.avatar_url)
                                }
                                requests.post(url=self.logs[str(payload.guild_id)], json=permissions)
                    elif payload.emoji.name == "✅":
                        if str(payload.guild_id) in self.logs:
                            accepted = {
                                "username": "Rules",
                                "embeds": [
                                    {
                                        "title": "Accepted:",
                                        "color": 65321,
                                        "fields": [
                                            {
                                                "name": "Username:",
                                                "value": str(member.mention),
                                                "inline": True
                                            },
                                            {
                                                "inline": True,
                                                "name": "User ID:",
                                                "value": str(member.id)
                                            }
                                        ],
                                        "footer": {
                                            "text": time_date() + " " + time_HMS()
                                        },
                                        "thumbnail": {
                                            "url": str(member.avatar_url)
                                        }
                                    }
                                ],
                                "avatar_url": str(self.bot.user.avatar_url)
                            }
                            requests.post(url=self.logs[str(payload.guild_id)], json=accepted)
                        try:
                            await member.add_roles(role)
                        except discord.errors.Forbidden:
                            if str(payload.guild_id) in self.logs:
                                permissions = {
                                    "username": "Rules",
                                    "embeds": [
                                        {
                                            "title": "Permissions:",
                                            "color": 16776960,
                                            "footer": {
                                                "text": time_date() + " " + time_HMS()
                                            },
                                            "description": "I am Missing Permissions to add role's"
                                        }
                                    ],
                                    "avatar_url": str(self.bot.user.avatar_url)
                                }
                                requests.post(url=self.logs[str(payload.guild_id)], json=permissions)

        @self.bot.event
        async def on_raw_reaction_remove(payload):
            if str(payload.guild_id) in self.messages:
                if payload.message_id == self.messages[str(payload.guild_id)]:
                    guild_id = payload.guild_id
                    guild = discord.utils.find(lambda g: g.id == guild_id, self.bot.guilds)
                    member = discord.utils.find(lambda m: m.id == payload.user_id, guild.members)
                    role = discord.utils.get(guild.roles, name="Player")
                    if payload.emoji.name == "✅":
                        if str(payload.guild_id) in self.logs:
                            Unaccepted = {
                                "username": "Rules",
                                "embeds": [
                                    {
                                        "title": "Unaccepted:",
                                        "color": 16711680,
                                        "fields": [
                                            {
                                                "name": "Username:",
                                                "value": str(member.mention),
                                                "inline": True
                                            },
                                            {
                                                "inline": True,
                                                "name": "User ID:",
                                                "value": str(member.id)
                                            }
                                        ],
                                        "footer": {
                                            "text": time_date() + " " + time_HMS()
                                        },
                                        "thumbnail": {
                                            "url": str(member.avatar_url)
                                        }
                                    }
                                ],
                                "avatar_url": str(self.bot.user.avatar_url)
                            }
                            requests.post(url=self.logs[str(payload.guild_id)], json=Unaccepted)
                        try:
                            await member.remove_roles(role)
                        except discord.errors.Forbidden:
                            if str(payload.guild_id) in self.logs:
                                permissions = {
                                    "username": "Rules",
                                    "embeds": [
                                        {
                                            "title": "Permissions:",
                                            "color": 16776960,
                                            "footer": {
                                                "text": time_date() + " " + time_HMS()
                                            },
                                            "description": "I am Missing Permissions to remove role's"
                                        }
                                    ],
                                    "avatar_url": str(self.bot.user.avatar_url)
                                }
                                requests.post(url=self.logs[str(payload.guild_id)], json=permissions)

        @self.bot.event
        async def on_ready():
            init()
            print(Fore.GREEN + Style.BRIGHT + "\n" +
                  " =======- " + str(time_date()) + " -=======\n" +
                  "  Bot Name: " + str(self.bot.user.name) + '#' + str(self.bot.user.discriminator) + "\n" +
                  "  Bot ID: " + str(self.bot.user.id) + "\n" +
                  "  Discord Version: " + str(discord.__version__) + "\n" +
                  " ========- " + str(time_HMS()) + " -========" +
                  Style.RESET_ALL + "\n")

        self.bot.run(self.token)


# run
if __name__ == '__main__':
    try:
        token = open(r'token.txt', 'r').read()
    except FileNotFoundError:
        log("you need to create a text file called token.txt with the bot token", "ERROR")
    except UnicodeDecodeError:
        log("token.txt has a false Decoding", "ERROR")
    else:
        prefix = '!'
        try:
            messages = json.loads(open("messages.json", "r").read())
        except json.decoder.JSONDecodeError:
            log("your messages.json in invalid", "ERROR")
        except UnicodeDecodeError:
            log("messages.json has a false Decoding", "ERROR")
        except FileNotFoundError:
            print("you need to create a file called messages.json with the content:\n"
                  "{\n"
                  '    "503969457945837589": 604995364235509760\n'
                  "}\n"
                  '//   server id: message id\n'
                  , "ERROR"
                  )
        else:
            try:
                logs = json.loads(open("logs.json", "r").read())
            except json.decoder.JSONDecodeError:
                log("your logs.json in invalid", "ERROR")
            except UnicodeDecodeError:
                log("logs.json has a false Decoding", "ERROR")
            except FileNotFoundError:
                log("you need to create a file called logs.json with the content:\n"
                    "{\n"
                    '      "503969457945837589": "https://discordapp.com/api/webhooks/{webhook.id}/{webhook.token}"\n'
                    "}\n"
                    '//    "server id": "webhook"'
                    , "ERROR"
                    )
            else:
                try:
                    Rules_bot(messages, logs, token).run()
                except discord.errors.LoginFailure:
                    log("your token is invalid", "ERROR")
